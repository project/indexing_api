<?php

namespace Drupal\indexing_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;

/**
 * The Class IndexSettingsForm.
 */
class IndexSettingsForm extends FormBase {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\Core\File\FileSystem definition.
   *
   * @var \Drupal\file\Entity\FileSystem
   */
  protected $fileSystem;

  /**
   * Drupal\file\FileUsage\FileUsageInterface definition.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityTypeBundleInfoInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Drupal\indexing_api\Service definition.
   *
   * @var \Drupal\indexing_api\Service
   */
  protected $indexService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->state = $container->get('state');
    $instance->fileSystem = $container->get('file_system');
    $instance->fileUsage = $container->get('file.usage');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->indexService = $container->get('indexing_api.index');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'index_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['container'] = [
      '#type' => 'fieldset',
      '#title' => t('Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    $this->state->get('i_hostname') != NULL ? $hostname = $this->state->get('i_hostname') : $hostname = \Drupal::request()->getSchemeAndHttpHost();

    $form['container']['i_hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname of the site'),
      '#description' => 'Enter the site hostname/domain.',
      '#default_value' => $hostname,
      '#required' => TRUE,
    ];

    $this->state->get('i_end_point') != NULL ? $i_end_point = $this->state->get('i_end_point') :
    $i_end_point = 'https://indexing.googleapis.com/v3/urlNotifications:publish';

    $form['container']['i_end_point'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint'),
      '#description' => 'Google indexing API Endpoint.',
      '#default_value' => $i_end_point,
      '#required' => TRUE,
    ];

    $this->state->get('i_scope') != NULL ? $i_scope = $this->state->get('i_scope') :
    $i_scope = 'https://www.googleapis.com/auth/indexing';

    $form['container']['i_scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client scope'),
      '#description' => 'Google client scope.',
      '#default_value' => $i_scope,
      '#required' => TRUE,
    ];

    $form['container']['i_json'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('The Client Service Account JSON File'),
      '#description' => 'JSON file containing the private/public key pair',
      '#default_value' => $this->state->get('i_json'),
      '#upload_location' => 'private://indexing-api/',
      '#upload_validators'  => [
        'file_validate_extensions' => ['json'],
      ],
      '#required' => TRUE,
    ];

    $form['indexing_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Indexing options'),
      '#description' => $this->t('Perform API requests for the selected entities'),
      '#weight' => '0',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $header = [
      'type' => $this->t('Entities'),
      'operations' => $this->t('Operations'),
    ];
    $form['indexing_options']['entity_types'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('There are no entity types.'),
    ];
    $applicableEntities = $this->state->get('i_entities');
    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type) {
      if ($this->indexService->isEntityTypeSupported($entity_type)) {
        $selected_bundles = [];
        foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type->id()) as $bundle_id => $bundle) {
          if (in_array($bundle_id, $applicableEntities[$entity_type->id()])) {
            $selected_bundles[$bundle_id] = $bundle['label'];
          }
        };

        $selected_bundles_list = [
          '#theme' => 'item_list',
          '#items' => $selected_bundles,
          '#context' => ['list_style' => 'comma-list'],
          '#empty' => $this->t('none'),
        ];

        $form['indexing_options']['entity_types'][$entity_type->id()] = [
          'type' => [
            '#type' => 'inline_template',
            '#template' => '<strong>{{ label }}</strong></br><span id="selected-{{ entity_type_id }}">{{ selected_bundles }}</span>',
            '#context' => [
              'label' => $this->t('@bundle types', ['@bundle' => $entity_type->getLabel()]),
              'entity_type_id' => $entity_type->id(),
              'selected_bundles' => $selected_bundles_list,
            ],
          ],
          'operations' => [
            '#type' => 'operations',
            '#links' => [
              'select' => [
                'title' => $this->t('Select'),
                'url' => Url::fromRoute('indexing_api.assign_form', [
                  'entity_type_id' => $entity_type->id(),
                ]
                ),
                'attributes' => [
                  'class' => ['use-ajax'],
                  'data-dialog-type' => 'modal',
                  'data-dialog-options' => Json::encode([
                    'width' => 700,
                  ]),
                  'data-ajax-wrapper' => 'selected-' . $entity_type->id(),
                ],
              ],
            ],
          ],
        ];
      };
    };

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_id = $form_state->getValue('i_json');
    $hostname = $form_state->getValue('i_hostname');
    $end_point = $form_state->getValue('i_end_point');
    $scope = $form_state->getValue('i_scope');

    // Make file permanent.
    if (isset($file_id[0])) {
      $file = $this->fileSystem->load($file_id[0]);
      if ($file !== NULL) {
        $file->status = FILE_STATUS_PERMANENT;
        $file->save();

        $uuid = $file->uuid();
        $this->fileUsage->add($file, 'indexing_api', 'indexing_api', $uuid);
      }
      else {
        $file_id = NULL;
      }
    }
    else {
      $file_id = NULL;
    }
    $this->state->set('i_json', $file_id);
    $this->state->set('i_hostname', $hostname);
    $this->state->set('i_end_point', $end_point);
    $this->state->set('i_scope', $scope);
  }

}
