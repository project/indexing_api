<?php

namespace Drupal\indexing_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * The Class AssignForm.
 */
class AssignForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityTypeBundleInfoInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->state = $container->get('state');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'assign_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    try {
      $this->entityType = $this->entityTypeManager->getDefinition($entity_type_id);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException();
    };
    $applicableEntities = $this->state->get('i_entities');
    $options = $defaults = [];
    foreach ($this->entityTypeBundleInfo->getBundleInfo($this->entityType->id()) as $bundle_id => $bundle) {
      $options[$bundle_id] = [
        'title' => ['data' => ['#title' => $bundle['label']]],
        'type' => $bundle['label'],
      ];
      if (in_array($bundle_id, $applicableEntities[$this->entityType->id()])) {
        $defaults[$bundle_id] = $bundle_id;
      };
    };

    if (!empty($options)) {
      $bundles_header = $this->t('All @entity_type types', ['@entity_type' => $this->entityType->getLabel()]);
      if ($bundle_entity_type_id = $this->entityType->getBundleEntityType()) {
        $bundles_header = $this->t('All @entity_type_plural_label', ['@entity_type_plural_label' => $this->entityTypeManager->getDefinition($bundle_entity_type_id)->getPluralLabel()]);
      }
      $form['entity_type_id'] = [
        '#type' => 'hidden',
        '#value' => $entity_type_id,
      ];
      $form['bundles'] = [
        '#type' => 'tableselect',
        '#header' => [
          'type' => $bundles_header,
        ],
        '#options' => $options,
        '#default_value' => $defaults,
        '#attributes' => ['class' => ['no-highlight']],
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => [$this, 'ajaxcallback'],
      ],
    ];
    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#value' => $this->t('Cancel'),
      '#ajax' => [
        'callback' => [$this, 'ajaxcallback'],
      ],
    ];

    return $form;
  }

  /**
   * Ajax callback to close the modal and update the selected text.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response object.
   */
  public function ajaxCallback() {
    $applicableEntities = $this->state->get('i_entities');
    $selected_bundles = [];
    foreach ($this->entityTypeBundleInfo->getBundleInfo($this->entityType->id()) as $bundle_id => $bundle) {
      if (in_array($bundle_id, $applicableEntities[$this->entityType->id()])) {
        $selected_bundles[$bundle_id] = $bundle['label'];
      };
    }
    $selected_bundles_list = [
      '#theme' => 'item_list',
      '#items' => $selected_bundles,
      '#context' => ['list_style' => 'comma-list'],
      '#empty' => $this->t('none'),
    ];
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    $response->addCommand(new HtmlCommand('#selected-' . $this->entityType->id(), $selected_bundles_list));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $applicableEntities = $this->state->get('i_entities');
    if (!is_array($applicableEntities[$form_state->getValue('entity_type_id')])) {
      $applicableEntities[$form_state->getValue('entity_type_id')] = [];
    };
    foreach ($form_state->getValue('bundles') as $bundle_id => $checked) {
      if ($checked) {
        array_push($applicableEntities[$form_state->getValue('entity_type_id')], $bundle_id);
      }
      else {
        if (($key = array_search($bundle_id, $applicableEntities[$form_state->getValue('entity_type_id')])) !== FALSE) {
          unset($applicableEntities[$form_state->getValue('entity_type_id')][$key]);
        };
      }
    };
    $applicableEntities[$form_state->getValue('entity_type_id')] = array_unique($applicableEntities[$form_state->getValue('entity_type_id')]);
    $this->state->set('i_entities', $applicableEntities);
  }

}
