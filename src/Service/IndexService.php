<?php

namespace Drupal\indexing_api\Service;

use Drupal\Core\State\StateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Google\Client;
use Drupal\file\Entity\File;

/**
 * The Class IndexService.
 */
class IndexService {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Entity\EntityTypeBundleInfoInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a new IndexService object.
   */
  public function __construct(StateInterface $state, LoggerChannelFactoryInterface $logger_factory, EntityTypeBundleInfoInterface $bundle_info) {
    $this->state = $state;
    $this->loggerFactory = $logger_factory;
    $this->entityTypeBundleInfo = $bundle_info;
  }

  /**
   * Performs a request to Google API.
   */
  public function performRequest($request_type, $url) {
    $hostname = $this->state->get('i_hostname');
    $json = $this->state->get('i_json');
    $scope = $this->state->get('i_scope');
    $end_point = $this->state->get('i_end_point');
    $content = [
      'type' => $request_type,
      'url' => $hostname . $url,
    ];
    $this->client = new Client();
    $json_file = File::load($json[0]);
    $this->client->setAuthConfig($json_file->getFileUri());
    $this->client->addScope($scope);
    $http_client = $this->client->authorize();
    $response = $http_client->post($end_point, ['body' => json_encode($content)]);
    $status_code = $response->getStatusCode();

    if ($status_code != 200) {
      $this->loggerFactory->error('Failed %request_type request for %url', [
        '%request_type' => $request_type,
        '%url' => $url,
      ]);
    }
    else {
      $this->loggerFactory->notice('Successfull %request_type request for %url', [
        '%request_type' => $request_type,
        '%url' => $url,
      ]);
    }

  }

  /**
   * Checks if an entity type is supported.
   */
  public function isEntityTypeSupported($entity_type) {

    $unsupported_types = [
      'block_content',
      'comment',
      'shortcut',
      'token_custom',
    ];

    // Check if it is an unsupported type.
    if (in_array($entity_type->id(), $unsupported_types)) {
      return FALSE;
    }

    // Allow only content entity type.
    if (!($entity_type instanceof ContentEntityTypeInterface)) {
      return FALSE;
    }

    // Do not allow internal entities.
    if ($entity_type->isInternal()) {
      return FALSE;
    }

    // Entity must have a canonical URL.
    if (!$entity_type->hasLinkTemplate('canonical') && !$entity_type->getUriCallback()) {
      return FALSE;
    }

    // Do not allow bundlable entities without bundles.
    if ($entity_type->getBundleEntityType() && !$this->entityTypeBundleInfo->getBundleInfo($entity_type->id())) {
      return FALSE;
    }

    return TRUE;

  }

  /**
   * Allows the API call for a certain entity type.
   *
   * @param $entity
   *   The entity.
   *
   * @return bool
   *   True/False.
   */
  public function isIndexable($entity) {
    $applicableEntities = $this->state->get('i_entities');
    if (array_key_exists($entity->getEntityTypeId(), $applicableEntities)) {
      if (in_array($entity->bundle(), $applicableEntities[$entity->getEntityTypeId()])) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
